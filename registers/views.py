import json
from django.core.serializers import serialize
from purchasesAPI.mixins import JsonResponseMixin
from django.views.generic import View
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render

from .models import Register

obj = Register.objects.get(id=1)

class JsonCBV(View):
    def get(self, request, *args, **kwargs):
        data = {
            "user": "rosana",
            "number": 125,
            "client": "Pedro Pérez",
            "description": "Venta de 3 teclados metodo cvb",
            "tax_rates": "16,00 %",
            "total_invoice": "69.600.000,00",
            "base": "60.000.000,00"
        }
        return JsonResponse(data)

class JsonCBV2(JsonResponseMixin, View):
    def get(self, request, *args, **kwargs):
        data = {
            "user": "rosana",
            "number": 125,
            "client": "Pedro Pérez",
            "description": "Venta de 3 teclados metodo cbv2",
            "tax_rates": "16,00 %",
            "total_invoice": "69.600.000,00",
            "base": "60.000.000,00"
        }
        return self.render_to_json_response(data)